import { _create_frames, _calculate_frame_score, Frame, calculateScore } from './../ScoreDPU'

describe('ScoreDPU', () => {

  describe('Frame', () => {
    it('should match snapshot', () => {
      const frame = new Frame()
      expect(frame).toMatchSnapshot()
    })
  })

  describe('_create_frames', () => {

    it('should calculate valid frames for valid 10 rolls, no spare, no strike', () => {
      const rolls = [
        2, 2,
        2, 2,
        2, 2,
        2, 2,
        2, 2,
        2, 2,
        2, 2,
        2, 2,
        2, 2,
        2, 2,
      ]
      const result = _create_frames(rolls)
      expect(result).toMatchSnapshot()
      expect(result).toHaveLength(10)
    })

    it('should calculate valid frames for valid 10 rolls, no spare, 2 strikes', () => {
      const rolls = [
        2, 2,
        2, 2,
        2, 2,
        2, 2,
        10,
        10,
        2, 2,
        2, 2,
        2, 2,
        2, 2,
      ]
      const result = _create_frames(rolls)
      expect(result).toMatchSnapshot()
      expect(result).toHaveLength(10)
    })

    it('should calculate valid frames for valid 10 rolls, 2 spares, no strike', () => {
      const rolls = [
        2, 2,
        2, 2,
        2, 2,
        2, 2,
        6, 4,
        2, 8,
        2, 2,
        2, 2,
        2, 2,
        2, 2,
      ]
      const result = _create_frames(rolls)
      expect(result).toMatchSnapshot()
      expect(result).toHaveLength(10)
    })

    it('should calculate valid frames for valid 10 rolls, 3 strikes in 10 round', () => {
      const rolls = [
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        10, 10, 10,
      ]
      const result = _create_frames(rolls)
      expect(result).toMatchSnapshot()
      expect(result).toHaveLength(12)
    })

    it('should calculate valid frames for valid 10 rolls, 1 spare in 10 round', () => {
      const rolls = [
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        6, 4, 2,
      ]
      const result = _create_frames(rolls)
      expect(result).toMatchSnapshot()
      expect(result).toHaveLength(11)
    })

    it('should calculate valid frames for valid 10 rolls, nohing on round 10', () => {
      const rolls = [
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
        0, 0,
      ]
      const result = _create_frames(rolls)
      expect(result).toMatchSnapshot()
      expect(result).toHaveLength(10)
    })
  })

  describe('_calculate_frame_score', () => {
    it('should calculate correct score for typical frame', () => {
      const frame = new Frame()
      const frames = [frame, new Frame(), new Frame()]
      frame.deliveries = [5, 2]
      frame.round = 0
      _calculate_frame_score(frame, frames)
      expect(frame.score).toEqual(7)
    })
  })

  describe('calculateScore', () => {
    it('should calculate rolls score - one spare', () => {
      /**
       * To not waster time I only mark this, there should be more tests of course, we can reuse ones from scoreController
       */
      expect(calculateScore([
        6, 4, 8, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      ])).toEqual(27)
    })
  })
})