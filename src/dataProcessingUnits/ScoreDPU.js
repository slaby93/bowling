
const MAX_FRAME_VALUE = 10
const LAST_ROUND = 12

/**
 * Frame class to keep unified structure
 */
export class Frame {
  deliveries = []
  score = null
  round = null
}

/**
 * Main function calculating rolls. Here we could use some DI to make testing more granular in future
 * @param {Array} rolls Array of numbers indicating rolls 
 */
export function calculateScore(rolls) {
  const frames = _create_frames(rolls)
  // Iterate from right to left - eliminates problem with waiting for score
  for (let index = frames.length - 1; index >= 0; --index) {
    const frame = frames[index]
    _calculate_frame_score(frame, frames)
  }

  const totalScore = frames.map(frame => frame.score).reduce((prev, next) => prev + next, 0)
  return totalScore
}

/**
 * Calculate score for particular frame
 * @param {Frame} frame  Frame
 * @param {Array} frames Array of Frames 
 */
export function _calculate_frame_score(frame, frames) {
  const [first, second = 0] = frame.deliveries

  frame.score = first + second
  if (frame.round >= 10) {
    return
  }

  const next_frame = frames[frame.round]
  const [first_next_frame_delivery] = next_frame.deliveries
  if (_isSpare(frame)) {
    frame.score = first_next_frame_delivery + MAX_FRAME_VALUE
    return
  }
  /**
   * Case: triple
   */
  if (_isStrike(frame) && _isStrike(next_frame)) {
    const next_frame_plus_2 = frames[next_frame.round]
    const [first_next_frame_plus_two_delivery] = next_frame_plus_2.deliveries
    frame.score = MAX_FRAME_VALUE + MAX_FRAME_VALUE + first_next_frame_plus_two_delivery
    return
  }
  if (_isStrike(frame)) {
    frame.score = MAX_FRAME_VALUE + next_frame.score
    return
  }
}

function _isStrike(frame) {
  return frame.deliveries[0] === MAX_FRAME_VALUE
}

function _isSpare(frame) {
  const [first, second] = frame.deliveries
  return first + second === MAX_FRAME_VALUE
}

/**
 * Chop rolls into frames
 * @param {Array} rolls Array of numbers indicating rolls 
 */
export function _create_frames(rolls) {
  const frames = []
  /**
   * Chop rolls into frames
   * I use for because sometimes we need to skip iteration - for example in case where
   * we take more then one delivery
   */
  for (let index = 0; index < rolls.length; index++) {
    const frame = new Frame()
    frame.round = frames.length + 1
    const first_delivery = rolls[index]
    frame.deliveries = [first_delivery]

    if (_isStrike(frame)) {
      frames.push(frame)
      continue
    }

    const second_delivery = rolls[++index]
    frame.deliveries.push(second_delivery)
    frames.push(frame)
  }

  return frames
}
