const Koa = require('koa')
const Router = require('koa-router')
const Logger = require('koa-logger')
const koaBody = require('koa-body');
import { initializeRoutes } from './routes/index'

const PORT = process.env.SERVER_PORT || 3000

export const app = new Koa()
export const router = new Router()

initializeRoutes(router)

app
  .use(koaBody())
  .use(Logger())
  .use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      ctx.status = err.status || 500;
      ctx.body = err.message;
      ctx.app.emit('error', err, ctx);
    }
  })
  .use(router.routes())
  .use(router.allowedMethods())

app.on('error', (err, ctx) => {
  /* centralized error handling:
   * We could store this information in sentry etc...
  */
});

export const server = app.listen(PORT, () => console.log(`Server listening on port ${PORT}`))