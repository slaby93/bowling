const scoreController = require('./../controllers/scoreController')

export const initializeRoutes = router =>
  /**
   * TL;DR;
   * Why using POST instead of GET?
   * Passing data as JSON will be easier with POST.
   * 
   * Some time ago we had discussion over should we send data over POST or GET ?
   * GET indicates that we want to fetch some data and provides us query string so we can pass informations but
   * in many cases that's not enough
   * POST inidicates we want to mutate, but allows us to pass safely data in nice, readable way.
   * 
   * I'm choosing this way as it will be just easier for you to pass data. Also, GraphQL is doing every request
   * regardless of if is it data fetch or mutation by POST. But I'm open for discussion.
   */
  router
    .post('/score', scoreController.post)
