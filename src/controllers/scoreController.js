const ScoreDPU = require('./../dataProcessingUnits/ScoreDPU')

/**
* @api {post} /score Request Bowling score calculation
* @apiName GetScore
* @apiGroup Score
* @apiDescription Returns calculated score for given sequence of valid sequence of rolls for one line of American Ten-Pin Bowling
* 
* @apiParam {Array} rolls of valid rolls.
* @apiParamExample {json} Request-Example:
               {"rolls":[10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]}
*
* @apiSuccess {Number} score Score for given sequence of rolls.
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "score": 300,
*     }
*/
export const post = async (ctx, next) => {
  const rolls = ctx.request.body.rolls
  try {
    ctx.response.body = JSON.stringify({
      score: ScoreDPU.calculateScore(rolls),
    })
  } finally {
    ctx.set('Content-type', 'application/json')
    await next()
  }
}
