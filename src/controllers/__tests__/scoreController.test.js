const Koa = require('koa')
const koaBody = require('koa-body')
const request = require('supertest')
const { post } = require('./../scoreController')

describe('POST /score', () => {

    describe('when given valid rolls sequence', () => {
        let koaInstance = null

        beforeAll(() => {
            koaInstance = new Koa()
            koaInstance
                .use(koaBody())
                .use(post)
        })

        it('should calculate valid score for 10 strikes (12 rolls: 10 pins knocked down on each roll)', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10
                    ]
                })
            expect(response.status).toEqual(200)
            expect(response.body.score).toEqual(300)
        })

        it('should calculate valid score for 20 rolls: 10 pairs of 9 and miss', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        9, 0, 9, 0, 9, 0, 9, 0, 9, 0,
                        9, 0, 9, 0, 9, 0, 9, 0, 9, 0
                    ]
                })
            expect(response.status).toEqual(200)
            expect(response.body.score).toEqual(90)
        })

        it('should calculate valid score for 21 rolls: 10 pairs of 5 and spare, with a final 5', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                        5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
                    ]
                })
            expect(response.status).toEqual(200)
            expect(response.body.score).toEqual(150)
        })

        it('should calculate valid score for 2 rolls, only basic scoring', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        1, 2,
                        4, 4,
                        0, 0,
                        0, 0,
                        0, 0,
                        0, 0,
                        0, 0,
                        0, 0,
                        0, 0,
                        0, 0,
                    ]
                })
            expect(response.status).toEqual(200)
            expect(response.body.score).toEqual(11)
        })

        it('should calculate valid score for 2 rolls, first strike, second basic', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        10, 7, 1, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0
                    ]
                })
            expect(response.status).toEqual(200)
            /**
             * We have strike here:
             * first frame: strike so 10 points, so we sum this and next frame score which is 10 + 8 = 18
             * second frame: 7 + 1 = 8 points
             * Total: 18 + 8 = 26
             */
            expect(response.body.score).toEqual(26)
        })

        it('should calculate valid score for 2 rolls, first spare, second basic', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        6, 4, 8, 1, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    ]
                })
            expect(response.status).toEqual(200)
            /**
             * We have spare so we take next result
             * first frame: 6 + 4 = 10 and because of spare, we have total score of 18 for first frame
             * second frame: 8 + 1 = 9
             * Total: 18 + 9 = 27
             */
            expect(response.body.score).toEqual(27)
        })

        it('should calculate valid score for 2 rolls, first spare, second strike', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        6, 4, 10, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    ]
                })
            expect(response.status).toEqual(200)
            expect(response.body.score).toEqual(30)
        })

        it('should calculate valid score for 2 rolls, first strike, second spare', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        10, 6, 4, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    ]
                })
            expect(response.status).toEqual(200)
            expect(response.body.score).toEqual(30)
        })

        it('should calculate valid score for 3 rolls, three strikes in a row', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        10, 10, 10, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0,
                    ]
                })
            expect(response.status).toEqual(200)
            expect(response.body.score).toEqual(60)
        })

        it('should calculate valid score for final rolls, three strikes at turn 10', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 10,
                    ]
                })
            expect(response.status).toEqual(200)
            /**
             * Strike at turn 10 gives us extra shot up to 3 shots
             * Each one was strike so we sum it up
             */
            expect(response.body.score).toEqual(30)
        })

        it('should calculate valid score for final rolls, spare at turn 10', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 6, 4, 2,
                    ]
                })
            expect(response.status).toEqual(200)
            /**
             * Spare at turn 10 gives us extra shot
             * Each one was strike so we sum it up
             */
            expect(response.body.score).toEqual(12)
        })

        it('should calculate valid score for final rolls, no extra shot at turn 10', async () => {
            const response = await request(koaInstance.listen())
                .post("/")
                .send({
                    rolls: [
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 2, 2,
                    ]
                })
            expect(response.status).toEqual(200)
            expect(response.body.score).toEqual(4)
        })
    })
})