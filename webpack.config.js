const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');

const config = {
  target: 'node',
  mode: 'production',
  entry: {
    main: [
      '@babel/polyfill',
      './src/index',
    ],
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(t|j)s?$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
      },
    ]
  },
  optimization: {
    minimizer: [new TerserPlugin({
      cache: false,
      parallel: true,
    })],
  },
  plugins: [new webpack.DefinePlugin({ "global.GENTLY": false })],
  resolve: {
    extensions: ['.ts', '.js',],
  },
  stats: {
    colors: true
  },
}

module.exports = config
