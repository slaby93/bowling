FROM node:11.14.0-alpine
COPY . /app
WORKDIR /app
RUN npm install
RUN npm run compile

FROM keymetrics/pm2:latest-alpine
COPY --from=0 /app/dist .
LABEL maintainer="pufek93@gmail.com"
EXPOSE 3000
EXPOSE 9615
CMD pm2-runtime main.js --web
