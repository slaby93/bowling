# Wunder Mobility - Bowling

Node.js based microservice for calculating score in bowling

[Scoring algorithm details](https://www.topendsports.com/sport/tenpin/scoring.htm)

[Wunder mobility task](./bowling_task.pdf)

## Start with Docker

```
docker build . -t wunder-bowling
docker run -p 3000:3000 -p 9615:9615 wunder-bowling
```

## How to install

In order to install dependencies, you need Node.js. In order to install it, you can use [nvm](https://github.com/creationix/nvm). Recommended version is `11.14.0`

```
npm install
```

## How to start application

```
npm run compile
npm start
```

## Documentation

In order to get documentation, just generate one!

```
npm run doc:generate
```

You can find it in `/doc` folder

## Development Tips

### Start development with Docker

This will start `docker` instance binded to port `3000` by default. You can change port with `PORT` environment variable.

```
docker-compose up --build
```

# Ideas how to improve:

- Add `sentry` integration ( or any other service )
- Add `husky` hooks to prevent maintainers from comitting invalid code
- Add `ESLint`
- Add type checking language (`TypeScript`/`Flow`)
- Add `Pact` tests
- Wire `CI` integration

# Docker size
With multi-stage build and minification I was able to decrease final docker image size to `112MB` for production ready full-size microservice.